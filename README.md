# Generated Documentation
## test_workflow3
Description: your role description


| Field                | Value           |
|--------------------- |-----------------|
| Functional description | Not available. |
| Requester            | Not available. |
| Users                | Not available. |
| Date dev             | Not available. |
| Date prod            | Not available. |
| Readme update            | 02/10/2023 |
| Version              | Not available. |
| Time Saving              | Not available. |
| Category              | Not available. |
| Sub category              | Not available. |


### Defaults

No defaults available.



### Vars

No vars available.


### Tasks
| Name | Module | Condition |
| ---- | ------ | --------- |
| Début du workflow sans variables | ansible.builtin.debug | False |
| Block 1 - Début | block | False |
| Block 1 - Tâche 1 | ansible.builtin.debug | False |
| Block 1 - Tâche 2 | ansible.builtin.debug | False |
| Block 2 - Début | block | False |
| Block 2 - Tâche 1 | ansible.builtin.debug | False |
| Block 2 - Tâche 2 | ansible.builtin.debug | False |
| Block 3 - Début | block | False |
| Block 3 - Tâche 1 | ansible.builtin.debug | False |
| Fin du workflow sans variables | ansible.builtin.debug | False |


## Task Flow Graphs

### Graph for main.yml
```mermaid
flowchart TD
  Start
  Start-->|Task| Début_du_workflow_sans_variables[début du workflow sans variables]
  Début_du_workflow_sans_variables-.->|End of Task| Début_du_workflow_sans_variables
  Début_du_workflow_sans_variables-->|Block Start| Block_1___Début_block_start_0[block 1   début]
  Block_1___Début_block_start_0-->|Task| Block_1___Tâche_1[block 1   tâche 1]
  Block_1___Tâche_1-->|Task| Block_1___Tâche_2[block 1   tâche 2]
  Block_1___Tâche_2-.->|End of Block| Block_1___Début_block_start_0
  Block_1___Tâche_2-->|Block Start| Block_2___Début_block_start_0[block 2   début]
  Block_2___Début_block_start_0-->|Task| Block_2___Tâche_1[block 2   tâche 1]
  Block_2___Tâche_1-->|Task| Block_2___Tâche_2[block 2   tâche 2]
  Block_2___Tâche_2-->|Block Start| Block_3___Début_block_start_1[block 3   début]
  Block_3___Début_block_start_1-->|Task| Block_3___Tâche_1[block 3   tâche 1]
  Block_3___Tâche_1-.->|End of Block| Block_3___Début_block_start_1
  Block_3___Tâche_1-.->|End of Block| Block_2___Début_block_start_0
  Block_3___Tâche_1-->|Rescue Start| Block_2___Début_rescue_start_0[block 2   début]
  Block_2___Début_rescue_start_0-->|Task| Une_erreur_s_est_produite_dans_Block_2[une erreur s est produite dans block 2]
  Une_erreur_s_est_produite_dans_Block_2-.->|End of Rescue Block| Block_2___Début_block_start_0
  Une_erreur_s_est_produite_dans_Block_2-->|Task| Fin_du_workflow_sans_variables[fin du workflow sans variables]
  Fin_du_workflow_sans_variables-.->|End of Task| Fin_du_workflow_sans_variables
```


## Playbook
```yml
---
- hosts: localhost
  remote_user: root
  roles:
    - test_workflow3

```
## Playbook graph
```mermaid
flowchart TD
  localhost-->|Role| test_workflow3[test workflow3]
```

## Author Information
your name

#### License
license (GPL-2.0-or-later, MIT, etc)

#### Minimum Ansible Version
2.1

#### Platforms
No platforms specified.